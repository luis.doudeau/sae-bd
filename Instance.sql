INSERT INTO Hotel VALUES ( 001, 'Relais Hôtel du Vieux Paris', '9 Rue Gît-le-Cœur, 75006 Paris', 289, 3);
INSERT INTO Hotel VALUES ( 002, 'Hôtel Le Chat Noir', '68 Bd de Clichy, 75018 Paris', 358, 4);
INSERT INTO Hotel VALUES (005, 'Hôtel Plaza Tour Eiffel' '32 Rue Greuze, 75116 Paris', 661, 4);
INSERT INTO Hotel VALUES (008, 'Hôtel du Collectionneur', '51-57 Rue de Courcelles, 75008 Paris', 487);

INSERT INTO Departement VALUES (001, '33', '1', 35);
INSERT INTO Departement VALUES (002, '46', '2', 51);
INSERT INTO Departement VALUES (005, '01', '2', 40);
INSERT INTO Departement VALUES (008, '86', '5', 110);

INSERT INTO Services VALUES (04, 'Services Restauration', '??', 11);
INSERT INTO Services VALUES (04, 'Services Restauration', '??', 6);
INSERT INTO Services VALUES (02, 'Service Téléphonique', '??', 0);
INSERT INTO Services VALUES (01, 'Accès SPA et services soins', '??', 20);

INSERT INTO Personne VALUES (60, 'DOUDEAU', '17 route de la forêt 45260 Lorris', to_date(2021));
INSERT INTO Personne VALUES (02, 'FAUCHER', '10 rue du parlement 75003 Paris ', to_date(2021));
INSERT INTO Personne VALUES (37, 'DOUDEAU', '17 route de la forêt 45260 Lorris', to_date(2021));
INSERT INTO Personne VALUES (99, 'DUPONT', '117 avenue de la résistance 75007 Paris', to_date(2021));

INSERT INTO Employe VALUES (60, 'Manager');
INSERT INTO Employe VALUES (02, 'Coach Sportif');
INSERT INTO Employe VALUES (99, 'Réceptionniste');




