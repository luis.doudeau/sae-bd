drop table Chambre;
drop table Departement;
drop table Services;
drop table Hotel;

create table Hotel(
    Id_Hotel number(3,0),
    Nom_Hotel varchar2(20),
    Adresse varchar2(50),
    Nbr_Chambre number(2,0)
    Etoile_Hotel number(1) check (Etoile_Hotel between 3 and 5)
    PRIMARY KEY Etoile_Hotel
    );

create table Chambre(
    Id_Hotel number(3,0),
    Nom_Chambre varchar2(2),
    Nombre_Max_Personne number(2,0),
    Superficie number(3,1),
    PRIMARY KEY (Id_Hotel,Nom_Chambre)
    FOREIGN KEY Id_Hotel REFERENCES Hotel(Id_Hotel));

create table Departement(
    Id_Hotel number(3,0),
    Nom_Departement varchar2(20),
    Responsable_Dep varchar2(20),
    NumPoste number(3,0),
    PRIMARY KEY (Id_Hotel, Nom_Departement),
    FOREIGN KEY Id_Hotel REFERENCES Hotel(Id_Hotel));

create table Services(
    Id_Service number(2,0),
    Nom_Service varchar2(20),
    Desc_Service varchar2(50),
    Prix_Service number(3,0),
    PRIMARY KEY Id_Service));

create table Personne(
    Id_Personne number(2,0),
    Nom_Personne varchar2(20),
    Adresse_Personne varchar2(50),
    DOB date,
);

create table Employe(
    Id_Personne number(2,0) REFERENCES Personne(Id_Personne)
);